package net.kotlincook

import io.ktor.serialization.jackson.*
import io.ktor.server.application.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.ktor.server.plugins.contentnegotiation.*
import net.kotlincook.db.DatabaseFactory
import net.kotlincook.repository.UserRepository
import net.kotlincook.repository.UserRepositoryImpl
import net.kotlincook.service.UserService
import net.kotlincook.service.UserServiceImpl
import net.simplifiedcoding.security.configureSecurity
import routes.authRoutes
import routes.checkRoutes

fun main() {
    embeddedServer(Netty, port = 8080, host = "127.0.0.1", module = Application::module)
        .start(wait = true)

}

fun Application.module() {
    DatabaseFactory.init()
    install(ContentNegotiation) {
        jackson()
    }
    configureSecurity()
    val service: UserService = UserServiceImpl()
    val repository: UserRepository = UserRepositoryImpl (service)
    authRoutes (repository)
    checkRoutes()
}
