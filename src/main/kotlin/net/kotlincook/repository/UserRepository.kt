package net.kotlincook.repository

import net.kotlincook.service.CreateUserParams
import net.kotlincook.utils.BaseResponse

interface UserRepository {
    suspend fun registerUser (params: CreateUserParams): BaseResponse<Any>
    suspend fun loginUser (email: String, password: String): BaseResponse<Any>
}