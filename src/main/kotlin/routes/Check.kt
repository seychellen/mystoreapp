package routes

import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Application.checkRoutes() {
    routing {
        authenticate {
            get("/testurl") {
                call.respond("Working Fine")
            }
        }
    }
}